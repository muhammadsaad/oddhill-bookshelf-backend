## Bookshelf Backend

The backend/API for Bookshelf is developed on Node, coupled with Typescript and Sequelize ORM.

### Usage

- Clone the repository
- Run `npm install`
- Create .env file with variables below <br/>
  `DB_NAME=` <br/>
  `DB_USERNAME=` <br/>
  `DB_PASSWORD=` <br/>
  `DB_HOST=localhost` <br/>
  `NODE_ENV=development` <br/>
  `JWT_SECRET=` <br/>
- Run `npm run dev` for development or `npm start` for production
- If you're using database apart from `mysql`, make sure to change the `dialect` in `src/Database/Sequelize.ts` <br/>
  The default port for the application is 3000, but it can be changed in `.env` file with variable `PORT`

### Routes

The API is prefixed with **/api** and below are the endpoints available

- #### Authentication
  The backend uses JWT authentcation and provides a jwt access token upon login. The validity of jwt is 24 hours and can be changed in `src/Controllers/Auth.ts`. <br/>

For the protected routes, the backend expects the jwt as Bearer token in header. <br/>
The login endpoint is `/api/auth/login`, and below users can be used for testing purpose since the backend doesn't support a database for users. <br/>

Username: **idtest0**, Password: **password0** <br/>
Username: **idtest1**, Password: **password1**

#### Endpooints

- #### Books

  - **GET** `/api/books` Return all books
  - **GET** `/api/books/:id` Return book with {id}
  - **GET** `/api/books/:id/authors` Return authors for book {id}
  - **GET** `/api/books/:id/genres` Return genres for book {id}
  - **GET** `/api/books/:id/details` Return details of book {id} from OpenLibrary

- #### Authors

  - **GET** `/api/authors` Return all authors
  - **GET** `/api/authors/:id` Return author with {id}
  - **GET** `/api/authors/:id/books` Return books for author {id}

- #### Genres

  - **GET** `/api/genres` Return all genres
  - **GET** `/api/genres/:id` Return genre with {id}

- #### Search
  The backend provides a search endpoint (case insensitive) for searching books by ISBN or title, authors by name and genres by name.
  - **GET** `/api/search/books/:term` Search and return books against isbn and title for the search term
  - **GET** `/api/search/authors/:term` Search and return authors matching name
  - **GET** `/api/search/genres/:term` Search and return genres against matching name
- #### Additional Endpoints
  The backend provides Post, Patch and Delete endpoints for creating, updating and deleting resources respectively. These end-points require authorization in form of Bearer header token. The backend expects the body params to be stringified for any post/patch request. Below are a bit more details of each endpoint and its function
  - ##### Books
    - **POST** `/api/books` To insert new book - This endpoint expects `title`, `isbn` and `description` (optional) in body params (stringified)
    - **PATCH** `/api/books/:id` To update book - This endpoint can receive `title`, `biography`, `description` in body params (stringified)
    - **DELETE** `/api/books/:id` To delete book
    - **PATCH** `/api/books/:id/assign-author/:author_id` To assign author to a book
    - **PATCH** `/api/books/:id/unassign-author/:author_id` To unassign author from a book
    - **PATCH** `/api/books/:id/assign-genre/:genre_id` To assign genre to a book
    - **PATCH** `/api/books/:id/unassign-author/:genre_id` To unassign genre from a book
  - ##### Authors
    - **POST** `/api/authors` To insert new author - This endpoint expects `name` and `biography` in body params (stringified)
    - **PATCH** `/api/authors/:id` To update author - This endpoint can receive `name` and/or `biography` in body params (stringified)
    - **DELETE** `/api/authors/:id` To delete author
  - ##### Genre
    - **POST** `/api/genres` To insert new genre - This endpoint expects `name` in body params (stringified)
    - **PATCH** `/api/genres/:id` To update genre - This endpoint expects `name` in body params (stringified)
    - **DELETE** `/api/genres/:id` To delete genre
- #### Future improvements
  I believe there's room for imporvement considering the scope of application and future aspects. Below are some of the points that I think the backend should have, but due either time contraint, they couldn't be completed for now.
  - Unit testing and functional testing should be implemented.
  - Models should be injected as dependency into services, just like we've injected logger. This will provide more ease in unit testing as well as when services are used in another cnotext.
  - As the application grows and more calls to third-party services are increased, we should have event handlers and listerners to remain intact the single resopnsiblity principle.
  - I was having trouble in establishing cascading (OnDelete Cascade) constraint on associations. That's the reason there are two queries for DELETE endpoints. I believe this should be reduced to one query.
  - Config vars can be loaded into single config alias module to avoid flodding process.env.BLA_BLA in the application. Since I was having a hard time setting up module alias to be compatible with TS compiler (see commit ba7ef87e), I put this for future development.
