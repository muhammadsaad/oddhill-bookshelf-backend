export interface OpenLibraryDetails {
  cover: string;
  subjects: string[];
  publish_date: string;
  publishers: string[];
  subjectPeople: string[];
}
