import express, { NextFunction, Request, Response } from "express";
import "express-async-errors";
import bodyParser from "body-parser";
import sequelize from "./Database/Sequelize";
import { createExpressServer, Action } from "routing-controllers";
import dependencyInjectorLoader from "./Loaders/dependencyInjector";
import jwt from "jsonwebtoken";

const app = createExpressServer({
  routePrefix: "/api",
  controllers: [__dirname + "/controllers/*"], // pass controllers' directory
  authorizationChecker: (action: Action) => {
    const token = action.request.headers.authorization;
    if (token) {
      const decoded = jwt.verify(token.split(" ")[1], process.env.JWT_SECRET);
      return decoded ? true : false;
    } else {
      return false;
    }
  },
});

/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true,
  })
);

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

/************************************************************************************
 *                              SEQUELIZE INIT
 ***********************************************************************************/

(async () => {
  await sequelize.sync();
  console.log("Sequelize synced");
})();

/************************************************************************************
 *                              DEPENDENCY INJECTION
 ***********************************************************************************/
dependencyInjectorLoader();

/************************************************************************************
 *                              APIs
 ***********************************************************************************/

app.get("/", (req: Request, res: Response) => {
  res.send("Your app.");
});

/************************************************************************************
 *                              Export Server
 ***********************************************************************************/

export default app;
