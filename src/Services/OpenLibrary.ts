import { Service, Inject, Container } from "typedi";
import { OpenLibraryDetails } from "../Interfaces/IBook";
import axios from "axios";
import { Logger } from "winston";

@Service()
export default class OpenLibraryService {
  constructor(
    @Inject("logger") private logger: Logger = Container.get("logger")
  ) {}

  public async getBookDetailsByISBN(
    isbn: string
  ): Promise<OpenLibraryDetails | Error> {
    this.logger.info(
      `Fetching book details from OpenLibrary for isbn: ${isbn}`
    );
    const url = `https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&format=json&jscmd=data`;
    try {
      const resp: any = Object.values((await axios.get(url)).data)[0];
      const details: OpenLibraryDetails = {
        cover: resp.cover.medium || "",
        publish_date: resp.publish_date,
        publishers: resp.publishers,
        subjectPeople: resp.subject_people,
        subjects: resp.subjects,
      };
      return details;
    } catch (e) {
      throw new Error(e);
    }
  }
}
