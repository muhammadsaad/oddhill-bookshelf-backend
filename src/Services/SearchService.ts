import { Service, Inject } from "typedi";
import Books from "../Database/Models/Books";
import Authors from "../Database/Models/Authors";
import Genres from "../Database/Models/Genres";
import { Op } from "sequelize";
import { Container } from "typedi";
import { Logger } from "winston";

@Service()
export default class SearchService {
  constructor(
    @Inject("logger") private logger: Logger = Container.get("logger")
  ) {}

  public searchBooks(term: string): Promise<Books[]> {
    this.logger.info(`Search books for term: ${term}`);
    try {
      const search = Books.findAll({
        where: {
          [Op.or]: [
            {
              title: {
                [Op.like]: `%${term}%`,
              },
            },
            {
              isbn: {
                [Op.like]: `%${term}%`,
              },
            },
          ],
        },
      });
      return search;
    } catch (e) {
      throw new Error(e);
    }
  }

  public searchAuthors(term: string): Promise<Authors[]> {
    this.logger.info(`Search authors for term: ${term}`);
    try {
      const search = Authors.findAll({
        where: {
          name: {
            [Op.like]: `%${term}%`,
          },
        },
      });
      return search;
    } catch (e) {
      throw new Error(e);
    }
  }

  public searchGenres(term: string): Promise<Genres[]> {
    this.logger.info(`Search genres for term: ${term}`);
    try {
      const search = Genres.findAll({
        where: {
          name: {
            [Op.like]: `%${term}%`,
          },
        },
      });
      return search;
    } catch (e) {
      throw new Error(e);
    }
  }
}
