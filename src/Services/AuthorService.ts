import { Service, Inject } from "typedi";
import Books from "../Database/Models/Books";
import Authors from "../Database/Models/Authors";
import Genres from "../Database/Models/Genres";
import AuthorsBooks from "../Database/Models/AuthorBooks";
import { Container } from "typedi";
import { Logger } from "winston";

@Service()
export default class AuthorService {
  constructor(
    @Inject("logger") private logger: Logger = Container.get("logger")
  ) {}
  public find(): Promise<Authors[]> {
    this.logger.info("Find all authors");
    try {
      const authors = Authors.findAll({
        order: [["name", "ASC"]],
      });
      return authors;
    } catch (e) {
      throw new Error(e);
    }
  }

  public findOne(id: number): Promise<Authors> {
    this.logger.info(`Find one author # ${id}`);
    try {
      const author = Authors.findOne({
        where: { id },
        include: [{ model: Books, through: { attributes: [] } }],
      });
      return author;
    } catch (e) {
      throw new Error(e);
    }
  }

  public findBooksOfOneAuthor(id: number): Promise<Books[]> {
    this.logger.info(`Find one author # ${id}, books only`);
    try {
      const books = Books.findAll({
        include: [{ model: Authors, where: { id }, attributes: [] }],
      });
      return books;
    } catch (e) {
      throw new Error(e);
    }
  }

  public insertOne(author: Authors): Promise<Genres> {
    this.logger.info("Calling Authors/insertOne with author: %o", author);
    try {
      return author.save();
    } catch (e) {
      throw new Error(e);
    }
  }

  public updateOne(id: number, author: Authors): Promise<number[]> {
    this.logger.info("Calling Authors/updateOne with author: %o", author);
    try {
      const toUpdate = {
        name: author.name,
        biography: author.biography,
      };
      console.log(id);
      console.log(toUpdate);
      return Authors.update(toUpdate, { where: { id } });
    } catch (e) {
      throw new Error(e);
    }
  }

  public deleteOne(id: number): Promise<number> {
    this.logger.info(`Calling Authors/deleteOne with id: ${id}`);
    try {
      AuthorsBooks.destroy({
        where: {
          author_id: id,
        },
      });
      return Authors.destroy({
        where: {
          id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }
}
