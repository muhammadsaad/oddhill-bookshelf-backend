import { Service, Inject } from "typedi";
import Books from "../Database/Models/Books";
import Authors from "../Database/Models/Authors";
import Genres from "../Database/Models/Genres";
import { Container } from "typedi";
import { Logger } from "winston";
import AuthorService from "./AuthorService";
import AuthorsBooks from "../Database/Models/AuthorBooks";
import BooksGenres from "../Database/Models/BooksGenres";
import GenreService from "./GenreService";

@Service()
export default class BookService {
  constructor(
    @Inject("logger") private logger: Logger = Container.get("logger")
  ) {}
  public find(): Promise<Books[]> {
    this.logger.info("Find all books");
    try {
      const books = Books.findAll({
        include: [
          { model: Authors, through: { attributes: [] } },
          { model: Genres, through: { attributes: [] } },
        ],
        order: [["title", "ASC"]],
      });
      return books;
    } catch (e) {
      throw new Error(e);
    }
  }

  public findOne(id: number): Promise<Books> {
    this.logger.info(`Find one book #${id}`);
    try {
      const book = Books.findOne({
        where: { id },
        include: [
          { model: Authors, through: { attributes: [] } },
          { model: Genres, through: { attributes: [] } },
        ],
      });
      return book;
    } catch (e) {
      throw new Error(e);
    }
  }

  public findAuthorsForOneBook(id: number): Promise<Authors[]> {
    this.logger.info(`Find one book #${id}, authors only`);
    try {
      const authors = Authors.findAll({
        include: [{ model: Books, where: { id }, attributes: [] }],
      });
      return authors;
    } catch (e) {
      throw new Error(e);
    }
  }

  public findGenresForOneBook(id: number): Promise<Genres[]> {
    this.logger.info(`Find one book #${id}, genres only`);
    try {
      const genres = Genres.findAll({
        include: [{ model: Books, where: { id }, attributes: [] }],
      });

      return genres;
    } catch (e) {
      throw new Error(e);
    }
  }

  public insertOne(book: Books): Promise<Books> {
    this.logger.info("Calling Books/insertOne with book: %o", book);
    try {
      return book.save();
    } catch (e) {
      throw new Error(e);
    }
  }

  public updateOne(id: number, book: Books): Promise<number[]> {
    this.logger.info("Calling Books/updateOne with book: %o", book);
    // TODOS: update genres and authors
    try {
      const toUpdate = {
        title: book.title,
        isbn: book.isbn,
        description: book.description,
      };
      console.log(id);
      console.log(toUpdate);
      return Books.update(toUpdate, { where: { id } });
    } catch (e) {
      throw new Error(e);
    }
  }

  public async assignAuthor(
    book_id: number,
    author_id: number
  ): Promise<[AuthorsBooks, boolean]> {
    this.logger.info(
      `Calling Books/assignAuthor with book_id: ${book_id}, author_id: ${author_id}`
    );
    try {
      const authorService = new AuthorService();
      const author = await authorService.findOne(author_id);
      const book = await this.findOne(book_id);
      if (!author) {
        throw new Error("Invalid author");
      }
      if (!book) {
        throw new Error("Invalid book");
      }
      return AuthorsBooks.findOrCreate({
        where: {
          author_id,
          book_id,
        },
        defaults: {
          book_id,
          author_id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  public async unassignAuthor(
    book_id: number,
    author_id: number
  ): Promise<number> {
    this.logger.info(
      `Calling Books/unassignAuthor with book_id: ${book_id}, author_id: ${author_id}`
    );
    try {
      const authorService = new AuthorService();
      const author = await authorService.findOne(author_id);
      const book = await this.findOne(book_id);
      if (!author) {
        throw new Error("Invalid author");
      }
      if (!book) {
        throw new Error("Invalid book");
      }
      return AuthorsBooks.destroy({
        where: {
          book_id,
          author_id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  public async assignGenre(
    book_id: number,
    genre_id: number
  ): Promise<[BooksGenres, boolean]> {
    this.logger.info(
      `Calling Books/assignGenre with book_id: ${book_id}, genre_id: ${genre_id}`
    );
    try {
      const genreService = new GenreService();
      const genre = await genreService.findOne(genre_id);
      const book = await this.findOne(book_id);
      if (!genre) {
        throw new Error("Invalid genre");
      }
      if (!book) {
        throw new Error("Invalid book");
      }
      return BooksGenres.findOrCreate({
        where: {
          genre_id,
          book_id,
        },
        defaults: {
          book_id,
          genre_id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  public async unassignGenre(
    book_id: number,
    genre_id: number
  ): Promise<number> {
    this.logger.info(
      `Calling Books/unassignGenre with book_id: ${book_id}, genre_id: ${genre_id}`
    );
    try {
      const genreService = new GenreService();
      const genre = await genreService.findOne(genre_id);
      const book = await this.findOne(book_id);
      if (!genre) {
        throw new Error("Invalid genre");
      }
      if (!book) {
        throw new Error("Invalid book");
      }
      return BooksGenres.destroy({
        where: {
          book_id,
          genre_id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  public deleteOne(id: number): Promise<number> {
    this.logger.info(`Calling Books/deleteOne with id: ${id}`);
    // TODOS: delete from AuthorsBook, Books Genres
    try {
      return Books.destroy({
        where: {
          id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }
}
