import { Service, Inject } from "typedi";
import Books from "../Database/Models/Books";
import Genres from "../Database/Models/Genres";
import { Container } from "typedi";
import { Logger } from "winston";
import BooksGenres from "../Database/Models/BooksGenres";

@Service()
export default class GenreService {
  constructor(
    @Inject("logger") private logger: Logger = Container.get("logger")
  ) {}
  public find(): Promise<Genres[]> {
    this.logger.info("Find all genres");
    try {
      const genres = Genres.findAll({
        order: [["name", "ASC"]],
      });
      return genres;
    } catch (e) {
      throw new Error(e);
    }
  }

  public findOne(id: number): Promise<Genres> {
    this.logger.info(`Find one genre #${id}`);
    try {
      const genre = Genres.findOne({
        where: { id },
        include: [{ model: Books, through: { attributes: [] } }],
      });
      return genre;
    } catch (e) {
      throw new Error(e);
    }
  }

  public insertOne(genre: Genres): Promise<Genres> {
    this.logger.info("Calling Genre/insertOne with genre: %o", genre);
    try {
      return genre.save();
    } catch (e) {
      throw new Error(e);
    }
  }

  public updateOne(id: number, genre: Genres): Promise<number[]> {
    this.logger.info("Calling Genre/updateOne with genre: %o", genre);
    try {
      const toUpdate = {
        name: genre.name,
      };
      console.log(id);
      console.log(toUpdate);
      return Genres.update(toUpdate, { where: { id } });
    } catch (e) {
      throw new Error(e);
    }
  }

  public deleteOne(id: number): Promise<number> {
    this.logger.info(`Calling Genre/deleteOne with id: ${id}`);
    try {
      BooksGenres.destroy({
        where: {
          genre_id: id,
        },
      });
      return Genres.destroy({
        where: {
          id,
        },
      });
    } catch (e) {
      throw new Error(e);
    }
  }
}
