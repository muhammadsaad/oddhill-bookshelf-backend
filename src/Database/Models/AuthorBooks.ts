import {
  Table,
  Column,
  Model,
  ForeignKey,
  PrimaryKey,
  AutoIncrement,
} from "sequelize-typescript";
import Books from "./Books";
import Authors from "./Authors";

@Table({
  timestamps: false,
  tableName: "authors_books",
})
export default class AuthorsBooks extends Model<AuthorsBooks> {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @ForeignKey(() => Books)
  @Column
  book_id!: number;

  @ForeignKey(() => Authors)
  @Column
  author_id!: number;
}
