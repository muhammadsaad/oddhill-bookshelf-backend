import {
  Table,
  Column,
  Model,
  PrimaryKey,
  BelongsToMany,
  AutoIncrement,
  Length,
} from "sequelize-typescript";
import Books from "./Books";
import BooksGenres from "./BooksGenres";

@Table({
  timestamps: false,
})
export default class Genres extends Model {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Length({ min: 4 })
  @Column
  name: string;

  @BelongsToMany(() => Books, () => BooksGenres)
  books?: Books[];
}
