import {
  Table,
  Column,
  Model,
  ForeignKey,
  PrimaryKey,
  AutoIncrement,
} from "sequelize-typescript";
import Books from "./Books";
import Genres from "./Genres";

@Table({
  timestamps: false,
  tableName: "books_genres",
})
export default class BooksGenres extends Model<BooksGenres> {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @ForeignKey(() => Books)
  @Column
  book_id!: number;

  @ForeignKey(() => Genres)
  @Column
  genre_id!: number;
}
