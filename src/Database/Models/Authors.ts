import {
  Table,
  Column,
  Model,
  PrimaryKey,
  BelongsToMany,
  AutoIncrement,
} from "sequelize-typescript";

import Books from "./Books";
import AuthorsBooks from "./AuthorBooks";

@Table({
  timestamps: false,
})
export default class Author extends Model {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Column
  name: string;

  @Column
  biography: string;

  @BelongsToMany(() => Books, () => AuthorsBooks)
  books?: Books[];
}
