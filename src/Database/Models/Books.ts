import {
  Table,
  Column,
  Model,
  PrimaryKey,
  BelongsToMany,
  Length,
  AutoIncrement,
  AllowNull,
} from "sequelize-typescript";
import Authors from "./Authors";
import AuthorsBooks from "./AuthorBooks";
import Genres from "./Genres";
import BooksGenres from "./BooksGenres";

@Table({
  timestamps: false,
})
export default class Books extends Model {
  @AutoIncrement
  @PrimaryKey
  @Column
  id: number;

  @Length({ min: 5 })
  @Column
  title: string;

  @Length({ min: 13, max: 13 })
  @Column
  isbn: string;

  @AllowNull(true)
  @Column
  description: string;

  @BelongsToMany(() => Authors, () => AuthorsBooks)
  authors?: Authors[];

  @BelongsToMany(() => Genres, () => BooksGenres)
  genres?: Genres[];
}
