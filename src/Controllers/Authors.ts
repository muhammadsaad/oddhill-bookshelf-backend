import {
  Controller,
  Param,
  Get,
  Authorized,
  Post,
  Body,
  Delete,
  Patch,
} from "routing-controllers";
import AuthorService from "../Services/AuthorService";
import Authors from "../Database/Models/Authors";

@Controller("/authors")
export class AuthorsController {
  @Get("/")
  async getAll() {
    try {
      const authorService = new AuthorService();
      const authors = await authorService.find();
      if (authors.length) {
        return authors.map((el) => el.get({ plain: true }));
      } else {
        return "No authors found";
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/:id")
  async getOne(@Param("id") id: number) {
    try {
      const authorService = new AuthorService();
      const author = await authorService.findOne(id);
      if (author) {
        return author.get({ plain: true });
      } else {
        return `No author found for #${id}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/:id/books")
  async getBooksForOne(@Param("id") id: number) {
    try {
      const authorService = new AuthorService();
      const books = await authorService.findBooksOfOneAuthor(id);
      if (books.length) {
        return books.map((el) => el.get({ plain: true }));
      } else {
        return `No books found for author #${id}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Post("/")
  async insertOne(@Body({ validate: true }) author: Authors) {
    try {
      const authorService = new AuthorService();
      return (await authorService.insertOne(author)).get({ plain: true });
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Patch("/:id")
  async updateOne(@Param("id") id: number, @Body() author: Authors) {
    try {
      const authorService = new AuthorService();
      if (await authorService.updateOne(id, author)) {
        return `Author #${id} updated`;
      }
      throw new Error(`Unable to update author #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Delete("/:id")
  async deleteOne(@Param("id") id: number) {
    try {
      const authorService = new AuthorService();
      if (await authorService.deleteOne(id)) {
        return `Author #${id} deleted`;
      }
      throw new Error(`Unable to delete author with #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }
}
