import { Controller, Param, Get } from "routing-controllers";
import SearchService from "../Services/SearchService";

@Controller("/search")
export class SearchController {
  // constructor() {}

  @Get("/books/:term")
  async search(@Param("term") term: string) {
    try {
      const searchService = new SearchService();
      const books = await searchService.searchBooks(term);
      if (books.length) {
        return books.map((el) => el.get({ plain: true }));
      } else {
        return `No books found for search term: ${term}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/authors/:term")
  async searchAuthors(@Param("term") term: string) {
    try {
      const searchService = new SearchService();
      const authors = await searchService.searchAuthors(term);
      if (authors.length) {
        return authors.map((el) => el.get({ plain: true }));
      } else {
        return `No authors found for search term: ${term}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/genres/:term")
  async searchGenres(@Param("term") term: string) {
    try {
      const searchService = new SearchService();
      const genres = await searchService.searchGenres(term);
      if (genres.length) {
        return genres.map((el) => el.get({ plain: true }));
      } else {
        return `No genres found for search term: ${term}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }
}
