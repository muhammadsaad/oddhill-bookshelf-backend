import {
  Controller,
  Param,
  Get,
  Post,
  Authorized,
  Delete,
  Patch,
  Body,
} from "routing-controllers";
import BookService from "../Services/BookService";
import OpenLibraryService from "../Services/OpenLibrary";
import { Container } from "typedi";
import { Logger } from "winston";
import Books from "../Database/Models/Books";

@Controller("/books")
export class BooksController {
  @Get("/")
  async getAll() {
    try {
      const bookService = new BookService();
      const books = await bookService.find();
      if (books.length) {
        return books.map((el) => el.get({ plain: true }));
      } else {
        return "No books found";
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/:id")
  async getOne(@Param("id") id: number) {
    try {
      const bookService = new BookService();
      const book = await bookService.findOne(id);
      if (book) {
        return book.get({ plain: true });
      } else {
        return "Book not found";
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/:id/authors")
  async getAuthorsOfBook(@Param("id") id: number) {
    try {
      const bookService = new BookService();
      const authors = await bookService.findAuthorsForOneBook(id);
      if (authors.length) {
        return authors.map((el) => el.get({ plain: true }));
      } else {
        return `No authors for book #${id}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/:id/genres")
  async getGenresOfBook(@Param("id") id: number) {
    try {
      const bookService = new BookService();
      const genres = await bookService.findGenresForOneBook(id);
      if (genres.length) {
        return genres.map((el) => el.get({ plain: true }));
      } else {
        return `No genres for book #${id}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/:id/details")
  async getDetails(@Param("id") id: number) {
    try {
      const logger: Logger = Container.get("logger");
      logger.info(`Get details for book #${id}`);
      const openlibraryService = new OpenLibraryService();
      const bookService = new BookService();
      const book = (await bookService.findOne(id)).get({ plain: true });
      const resp = await openlibraryService.getBookDetailsByISBN(book.isbn);
      return resp;
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Post("/")
  async insertOne(@Body({ validate: true, required: true }) book: Books) {
    try {
      const bookService = new BookService();
      return (await bookService.insertOne(book)).get({ plain: true });
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Patch("/:id")
  async updateOne(
    @Param("id") id: number,
    @Body({ validate: true, required: true }) book: Books
  ) {
    try {
      const bookService = new BookService();
      if (await bookService.updateOne(id, book)) {
        return `Book #${id} updated`;
      }
      throw new Error(`Unable to update book #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Patch("/:id/assign-author/:author_id")
  async assignAuthor(
    @Param("id") id: number,
    @Param("author_id") author_id: number
  ) {
    try {
      const bookService = new BookService();
      const res = await bookService.assignAuthor(id, author_id);
      console.log(res);
      if (!res[1]) {
        return `Book #${id} already has author ${author_id}`;
      }
      if (res) {
        return `Book #${id} updated, author ${author_id} assigned`;
      }
      throw new Error(`Unable to update book #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Patch("/:id/unassign-author/:author_id")
  async unassignAuthor(
    @Param("id") id: number,
    @Param("author_id") author_id: number
  ) {
    try {
      const bookService = new BookService();
      if (await bookService.unassignAuthor(id, author_id)) {
        return `Book #${id} updated, author ${author_id} unassigned`;
      }
      throw new Error(`Unable to update book #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Patch("/:id/assign-genre/:genre_id")
  async assignGenre(
    @Param("id") id: number,
    @Param("genre_id") genre_id: number
  ) {
    try {
      const bookService = new BookService();
      const res = await bookService.assignGenre(id, genre_id);
      if (!res[1]) {
        return `Book #${id} already has genre ${genre_id}`;
      }
      if (res) {
        return `Book #${id} updated, genre ${genre_id} assigned`;
      }
      throw new Error(`Unable to update book #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Patch("/:id/unassign-genre/:genre_id")
  async unassignGenre(
    @Param("id") id: number,
    @Param("genre_id") genre_id: number
  ) {
    try {
      const bookService = new BookService();
      if (await bookService.unassignGenre(id, genre_id)) {
        return `Book #${id} updated, genre ${genre_id} unassigned`;
      }
      throw new Error(`Unable to update book #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Delete("/:id")
  async deleteOne(@Param("id") id: number) {
    try {
      const bookService = new BookService();
      if (await bookService.deleteOne(id)) {
        return `Book #${id} deleted`;
      }
      throw new Error(`Unable to delete book with #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }
}
