/* tslint:disable:max-classes-per-file */
import { Controller, Post, Body } from "routing-controllers";
import { MinLength } from "class-validator";
import jwt from "jsonwebtoken";

class User {
  @MinLength(4)
  username: string;

  @MinLength(4)
  password: string;
}

@Controller("/auth")
export default class AuthController {
  @Post("/login")
  login(@Body({ validate: true }) user: User) {
    try {
      const users = [
        { username: "idtest0", password: "password0" },
        { username: "idtest1", password: "password1" },
      ];
      const found = users.find(
        (u) => u.username === user.username && u.password === user.password
      );
      if (found) {
        const payload = {
          username: user.username,
        };
        const accessToken = jwt.sign(payload, process.env.JWT_SECRET, {
          algorithm: "HS256",
          expiresIn: 60 * 60 * 24, // one day
        });

        return {
          user: payload,
          accessToken,
        };
      } else {
        return "Invalid credentials";
      }
    } catch (e) {
      throw new Error(e);
    }
  }
}
