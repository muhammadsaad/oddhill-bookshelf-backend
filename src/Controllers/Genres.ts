import {
  Controller,
  Param,
  Get,
  Post,
  Delete,
  Patch,
  Authorized,
  Body,
} from "routing-controllers";
import GenreService from "../Services/GenreService";
import Genres from "../Database/Models/Genres";

@Controller("/genres")
export class GenresController {
  @Get("/")
  async getAll() {
    try {
      const genreService = new GenreService();
      const genres = await genreService.find();
      if (genres.length) {
        return genres.map((el) => el.get({ plain: true }));
      } else {
        return "No genres found";
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Get("/:id")
  async getOne(@Param("id") id: number) {
    try {
      const genreService = new GenreService();
      const genre = await genreService.findOne(id);
      if (genre) {
        return genre.get({ plain: true });
      } else {
        return `No genre found with #${id}`;
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Post("/")
  async insertOne(@Body({ validate: true }) genre: Genres) {
    try {
      const genreService = new GenreService();
      return (await genreService.insertOne(genre)).get({ plain: true });
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Patch("/:id")
  async updateOne(@Param("id") id: number, @Body() genre: Genres) {
    try {
      const genreService = new GenreService();
      if (await genreService.updateOne(id, genre)) {
        return `Genre #${id} updated`;
      }
      throw new Error(`Unable to update genre #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }

  @Authorized()
  @Delete("/:id")
  async deleteOne(@Param("id") id: number) {
    try {
      const genreService = new GenreService();
      if (await genreService.deleteOne(id)) {
        return `Genre #${id} deleted`;
      }
      throw new Error(`Unable to delete genre with #${id}`);
    } catch (e) {
      throw new Error(e);
    }
  }
}
